open Client_state
open Gui_common
open Utils

let load_file file =
  let load = ref true in
  (try
    _log ("loading " ^ file);
    gs := Some(new Game_state.game_state (Game_state.JSON file));
    filename := file;
  with
    Failure s
  | Sys_error s ->
     load := false;
     let msg = ("Erreur de chargement : " ^ s) in
     _log msg;
     st_pop(); st_flash ~delay:7000 msg
  | _ ->
     load := false;
     let msg = "Erreur de chargement" in
     _log msg;
     st_pop(); st_flash ~delay:7000 msg);
  if (!load)
  then !load_game()

let chose_game () =
  st_push "Chargement d'un nouveau jeu...";
  let filew = GWindow.file_chooser_dialog
      ~action:`OPEN
      ~title:"Ouvrir un fichier" ~border_width:0
      ~width:320 ~height:240
      () in
  filew#add_filter (GFile.filter ~name:"json" ~patterns:["*.json"] ());
  filew#add_button_stock `CANCEL `CANCEL;
  filew#add_select_button_stock `OPEN `OPEN;
  begin match filew#run(), filew#filename with
    | `OPEN, Some filename ->
      filew#destroy ();
      load_file filename;
    | _ -> filew#destroy ();
      st_pop();
  end

let reload_game () =
  if !filename = ""
  then st_flash "Impossible de recharger la partie"
  else load_file !filename
