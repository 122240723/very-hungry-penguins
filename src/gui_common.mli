(** Common functions and variables for GUI *)

val status : GMisc.statusbar
val window : GWindow.window

(** Print a message in the status bar of the main window. *)
val st_push : string -> unit

(** Flash a message in the status bar of the main window.
 * Default duration: 5000ms *)
val st_flash : ?delay:int -> string -> unit

(** Flush status bar of the main window. *)
val st_pop : unit -> unit

(** Get the current game_state from client_state module *)
val get_gs : unit -> Game_state.game_state

(** on pourrait faire mieux *)
val load_game : (unit -> unit) ref
